// Created by bharat khanna on 7/6/17.
// Fibonacci formula : arr[n] = recursion(arr[n-1]) + recursion(arr[n-2]);
// for checking prime number Sieve of Eratosthenes theorem used
//"Buzz" when F(n) is divisible by 3.
//"Fizz" when F(n) is divisible by 5.
//"FizzBuzz" when F(n) is divisible by 15.
//"BuzzFizz" when F(n) is prime.
//the value F(n) otherwise.

#define without_recursion 1

#if without_recursion

#include<stdio.h>
#include<stdlib.h>  // for malloc usage
#include<stdbool.h> // bool
#include<math.h>    //sqrt
bool only3(int temp);
bool only5(int temp);
bool only15(int temp);
bool prime(int temp);

int *arr;
bool *a;
void fib(int n)
{
    arr = (int *)malloc( n * sizeof(int) );
    static int i =0;
    while(i <= n)
    {
        if (i == 0)
            arr[i] = 0;
        else if(i == 1 || i ==2)
            arr[i] = 1;
        else
            arr[i] = arr[i-1] + arr[i-2];
        i+=1;
    }
}

int main ()
{
    int n;
    printf("Enter the number = ");
    scanf("%d", &n);
    fib(n);
    for(int i = 0 ;  i <= n ; i++)
    {
        if(i == 0 || i == 1  || i ==2)
            printf("%d", arr[i]);
        else if( only3(arr[i]) )
            printf("Buzz");
        else if( only5(arr[i]) )
            printf("Fizz");
        else if( only15(arr[i]) )
            printf("FizzBuzz");
        else if( !prime(arr[i]) )
            printf("BuzzFizz");
        else
            printf("%d", arr[i]);
        printf(" ");
    }
    printf("\n");
    /*
    for(int i = 0 ;  i <= n ; i++)
    {
        if(i == 0 || i == 1  || i ==2)
            printf("%d", arr[i]);
        else if( only3(arr[i]) )
            printf("%d  Buzz",arr[i]);
        else if( only5(arr[i]) )
            printf("%d  Fizz",arr[i]);
        else if( only15(arr[i]) )
            printf("%d  FizzBuzz",arr[i]);
        else if( !prime(arr[i]) )
            printf("%d  BuzzFizz",arr[i]);
        else
            printf("%d", arr[i]);
        printf("\n");
    }
     */
    return 0;
}

bool only3(int temp)
{
    if( (temp % 3 == 0) && !(temp % 15 == 0))
        return true;
    return false;
}
bool only5(int temp)
{
    if( (temp % 5 == 0) && !(temp % 15 == 0))
        return true;
    return false;
}
bool only15(int temp)
{
    if(temp % 15 == 0)
        return true;
    return false;
}
bool prime(int temp)
{
    a = (bool *)malloc(temp * sizeof(bool));
    for(int i = 2; i < temp ; i++)
        a[i] = false;
    a[0] =a[1] = true;
    for(int i = 2; i < sqrt(temp) ; i++)
    {
        if(a[i] == false)
        {
            for(int j = i+i; j <= temp ; j+=i)
            {
                a[j] = true;
            }
        }
    }
    return a[temp];
}

#endif
